1. Git Clone https://gitlab.com/Krisnasw/laundree
2. cp .env.example .env
3. composer install
4. php artisan key:generate
5. php artisan migrate --seed
6. php artisan serve